package cdp.leolaz.jbehave.pageobjects;

import org.openqa.selenium.support.PageFactory;

import static cdp.leolaz.jbehave.utils.WebDriverUtils.getDriver;

public abstract class PageObject {

    public PageObject(){
        PageFactory.initElements(getDriver(), this);
    }
}
