package cdp.leolaz.jbehave.pageobjects;

import cdp.leolaz.jbehave.utils.WebDriverUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static cdp.leolaz.jbehave.utils.WebDriverUtils.newWait;

public class GMainPage extends PageObject {

    Logger logger = LogManager.getLogger(GMainPage.class);

    @FindBy(css = "input[name = 'q']")
    private WebElement searchInput;

    @FindBy(css = "input[name = 'btnI']")
    private WebElement luckyBtn;

    @FindBy(css = "input[name = 'btnK']")
    private WebElement searchBtn;

    @FindBy(css ="#logo > img")
    private WebElement googleLogo;

    public void fillSearchInput(String q){
        newWait().until(ExpectedConditions.visibilityOf(searchInput));
        searchInput.sendKeys(q);
        logger.info(String.format("Fill search input with '%s'", q));
    }

    public void clickSearchBtn(){
        newWait().until(ExpectedConditions.visibilityOf(searchBtn));
        searchBtn.click();
        logger.info("Click on search button");
    }

    public void clickLuckyBtn(){
        newWait().until(ExpectedConditions.visibilityOf(luckyBtn));
        luckyBtn.click();
        logger.info("Click on lucky button");
    }

    public String getTitle(){
        newWait().until(ExpectedConditions.visibilityOf(googleLogo));
        return WebDriverUtils.getDriver().getTitle();
    }
}
