package cdp.leolaz.jbehave.steps;

import cdp.leolaz.jbehave.pageobjects.GMainPage;
import cdp.leolaz.jbehave.utils.WebDriverUtils;
import org.jbehave.core.annotations.*;
import org.jbehave.core.steps.Steps;
import org.junit.Assert;

public class First extends Steps{
    private GMainPage gMainPage;
    private String q;

    @BeforeStory
    public void setUp(){

    }

    @Given("I open a browser")
    public void openBrowser() {
        WebDriverUtils.getDriver();
        System.out.println("Open browser");
    }

    @When("I enter $url")
    public void enterUrl(String url){
        WebDriverUtils.load(url);
        //Assert.assertEquals("URL is not the same" , url, WebDriverUtils.getDriver().getCurrentUrl());
        System.out.println("enter");
    }

    @When("I type $q")
    public void q(String q){
        this.q = q;
        gMainPage = new GMainPage();
        gMainPage.fillSearchInput("test");
        gMainPage.clickSearchBtn();
    }

    @Then("Google is displayed")
    public void display(){
        String title = WebDriverUtils.getDriver().getTitle();
        boolean condition = title.contains("Google") && title.contains(q);
        Assert.assertTrue(gMainPage.getTitle().contains("Google"));
        System.out.println("displayed");
    }

    @AfterStory
    public void cleanUp(){
        WebDriverUtils.close();
    }

}