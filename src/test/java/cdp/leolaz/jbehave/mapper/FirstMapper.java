package cdp.leolaz.jbehave.mapper;

import cdp.leolaz.jbehave.steps.First;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.failures.SilentlyAbsorbingFailure;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.CandidateSteps;
import org.jbehave.core.steps.InstanceStepsFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FirstMapper extends JUnitStories {

    public FirstMapper(){
        super();
        this.configuredEmbedder().candidateSteps().add(new First());
    }

    @Override
    public Configuration configuration(){
        return new MostUsefulConfiguration()/*.useStoryLoader(new LoadFromClasspath(getClass().getClassLoader()))*/
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withFormats(Format.XML, Format.STATS, Format.CONSOLE, Format.HTML)
                        .withRelativeDirectory("../build/jbehave")
                )
                .usePendingStepStrategy(new FailingUponPendingStep())
                .useFailureStrategy(new SilentlyAbsorbingFailure());
    }

    @Override
    public List<CandidateSteps> candidateSteps(){
        return new InstanceStepsFactory(configuration(), this).createCandidateSteps();
    }

    @Override
    protected List<String> storyPaths() {
        //return Collections.singletonList("stories/first.story");
        String storyToInclude = "**/" + System.getProperty("story", "*")
                + "*grad.story";
        return new StoryFinder().findPaths(
                CodeLocations.codeLocationFromClass(this.getClass()).getFile().replace("/classes", "/resources"),storyToInclude, "");
    }
}
